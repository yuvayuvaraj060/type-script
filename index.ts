// Number Type
let age = 24;
let weight: number = 70; // tell the ts to im going to use a only number in weight
weight = 100; // it's fine
weight = "70"; // this is wrong type beaus the var type is not a string
/**
 * The TS is infer the types, its usefully for catch the error and typos in the dev stage. */

// Type string
let myName = "Yuvi";
myName = 12132; // TS is yelling me that u not able to assign the string value to number...

// Type boolean
let isAwesome = false;
isAwesome = ""; // error is that is not able to assign the string to a boolean value.

// Types Interface

// Object

const Obj = {
  firstName: "John",
  lastName: "Mon",
  phone: "1234567890",
  age: 21,
  gender: true, //true  == male, false == female
};

const Obj1: {
  firstName: string;
  lastName: string;
  phone: string;
  age: number;
  gender: boolean; //true  == male, false == female
} = {
  firstName: "John",
  lastName: "Mon",
  phone: "1234567890",
  age: 21,
  gender: true,
};

// array

const array = [[[""], ""], ""];

// Function

const add = (n1: number, n2: number = 1): number => {
  return n1 + n2;
};

add(1);

const log = (): void => {
  console.log("hey");
};

// interface

// interface export is very help full for using other places for same time for typing things
export interface User {
  id: number;
  name: string;
  dod: string;
  phone: string;
  age: number;
  gender: string;
  email: string;
}

//  interface of interface
/*Here the interface is dose is that using other interface for refer for creating array of users */
export interface Users {
  users: User[];
}

// Types
/*type can be any types  useful for creating multiple types */

type userOrUsers = User | Users;
// the type shoes the structure os the type but the interface shows only the interface and thats name
type post = {
  id: number;
  title: string;
  body: string;
  upVote: number;
};

// typed functions
function typedFunc(num1: number, num2: number): number {
  return num1 + num2;
}

const create3Posters = (post: post): [post, post, post] => {
  return [post, post, post];
};

// enums
/*Enums are very useful to create  action creator types */

export enum getUserActionType {
  GET_USER = "GET_USER",
  LOAD_USER = "LOAD_USER",
  ERROR = "ERROR",
  DONE = "DONE",
}

// getUserActionType.LOAD_USER

// class

export interface OnInit {
  onInit(): string;
}

class TetsClass implements OnInit {
  onInit() {
    return "hai";
  }
}

// class member with private, public and static specification

class Members {
  private num: number; // the private property is not able to use and extend on the other class
  public num1: number; // the public member property can use out side of the class also
  num3: number; // also can use other class and extend
  static num2: number; // the static member property is stays same can't create by new key word rather we can access directly like a Obj
  protected num4: number; // only available on sub classes

  constructor() {} // constructor is a method in the class js on the compile time this method will call and executes what's inside the constructor

  main(name: string): string {
    return `Hello${name}`;
  }
}
