"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.getUserActionType = void 0;
// Number Type
let age = 24;
let weight = 70; // tell the ts to im going to use a only number in weight
weight = 100; // it's fine
weight = "70"; // this is wrong type beaus the var type is not a string
/**
 * The TS is infer the types, its usefully for catch the error and typos in the dev stage. */
// Type string
let myName = "Yuvi";
myName = 12132; // TS is yelling me that u not able to assign the string value to number...
// Type boolean
let isAwesome = false;
isAwesome = ""; // error is that is not able to assign the string to a boolean value.
// Types Interface
// Object
const Obj = {
    firstName: "John",
    lastName: "Mon",
    phone: "1234567890",
    age: 21,
    gender: true, //true  == male, false == female
};
const Obj1 = {
    firstName: "John",
    lastName: "Mon",
    phone: "1234567890",
    age: 21,
    gender: true,
};
// array
const array = [[[""], ""], ""];
// Function
const add = (n1, n2 = 1) => {
    return n1 + n2;
};
add(1);
const log = () => {
    console.log("hey");
};
// typed functions
function typedFunc(num1, num2) {
    return num1 + num2;
}
const create3Posters = (post) => {
    return [post, post, post];
};
// enums
/*Enums are very useful to create  action creator types */
var getUserActionType;
(function (getUserActionType) {
    getUserActionType["GET_USER"] = "GET_USER";
    getUserActionType["LOAD_USER"] = "LOAD_USER";
    getUserActionType["ERROR"] = "ERROR";
    getUserActionType["DONE"] = "DONE";
})(getUserActionType = exports.getUserActionType || (exports.getUserActionType = {}));
class TetsClass {
    onInit() {
        return "hai";
    }
}
// class member with private, public and static specification
class Members {
    constructor() { } // constructor is a method in the class js on the compile time this method will call and executes what's inside the constructor
    main(name) {
        return `Hello${name}`;
    }
}
//# sourceMappingURL=index.js.map